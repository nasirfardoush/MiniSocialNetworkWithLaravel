<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use Auth;
class ArticleController extends Controller
{
   
    public function index()
    {
       $articles =  Article::paginate(10);
       return view('articles.index',compact('articles'));
    }

   
    public function create()
    {
        return view('articles.create');
    }

    
    public function store(Request $request)
    {
        /* 
        *   This is the first way insert data into table by only this code
        *
       $article = new Article;
        $article->user_id = Auth::user()->id;
        $article->content =$request->content;
        $article->live    = (boolean)$request->live;
        $article->post_on    = $request->post_on;
        $article->save();
        */

        //Second is elequent mapping and its need for hidden id input
        //Article::create($request->all());

        //Third is my choices
        Article::create([
            'user_id'=>Auth::user()->id,
            'content'=>$request->content,
            'live'   =>$request->live,
            'post_on' =>$request->post_on

        ]);


    }

   
    public function show($id)
    {
        $article = Article::find($id);
        if(!empty($article)){
             return view('articles.show',compact('article'));
        }else{
            return "Data Not Found";
        }
        
    }

    
    public function edit($id)
    {
        $article = Article::find($id); 
        return view('articles.edit',compact('article'));

    }

    public function update(Request $request, $id)
    {

        $article = Article::find($id);
        $article->user_id = Auth::user()->id;
        $article->content =$request->content;
        $article->live    = (boolean)$request->live;
        $article->post_on    = $request->post_on;
        $article->save();


    }

    public function destroy($id)
    {
        //
    }
}
