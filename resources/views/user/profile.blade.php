@extends('layouts.app')

<style>
	.profile_img{
		max-width: 125px;
		border:5px solid #fff;
		border-radius: 100%;
		box-shadow: 0 2px 2px rgba(0,0,0,0.3);
	}
</style>

@section('content')
	<div class="row">
		<div class="col-md-6 col-lg-6 col-md-offset-3 co-lg-offset-3" >
			<div class="panel panel-defult">
				<div class="panel-body text-center" >
					<img class="profile_img"src="http://previews.123rf.com/images/prettyvectors/prettyvectors1309/prettyvectors130900060/22545994-User-Profile-Avatar-Man-Icon-Stock-Vector.jpg">

					<h3>{{ $user->name }}</h3>
					<h5>{{ $user->email }}</h5>

	{{--....................Heare calculate age using by custom date mutator --}}
					<h5>{{ $user->dateOfBirth->format('l j F Y') }} ({{ $user->dateOfBirth->age  }} years old) </h5>
					<button  class="btn btn-success" >Follow</button>
				</div>
			</div>
		</div>
	</div>
@endsection