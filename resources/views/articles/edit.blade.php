@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-md-6 col-lg-6 col-md-offset-3 co-lg-offset-3" >
			<div class="panel panel-defult">
				<div class="panel panel-heading text-center">
					Edit Article
					</div>
				<div class="panel-body" >
					<form action="/articles/{{ $article->id }}" method="POST">
						
						{{ method_field('PUT') }}
						{{ csrf_field() }}
						
						<div class="form-group" >
							<label for="content" >Content</label>
							<textarea name="content" class="form-control" >
								{{ $article->content }}
							</textarea>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" {{ $article->live == 1 ?'checked':''}} name="live">
								live
							</label>
							
						</div>
						<div class="form-group">
							<label for="post_on">Post on</label>
							<input type="datetime-local" value="{{ $article->post_on->format('Y-m-d\TH:i:s') }}" name="post_on" class="form-control">
						</div>
						<div>
							<input type="submit" name="submit" value="Update" class="btn btn-success pull-right">
						</div>
					</form>
				</div>
		</div>
	</div>
@endsection