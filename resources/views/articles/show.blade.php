@extends('layouts.app')

@section('content')
		<div class="row">
			<div class="col-md-4 col-lg-4 col-md-offset-4 co-lg-offset-4" >
				<div class="panel panel-defult">
					<div class="panel panel-heading text-center clearfix">
						<span class="pull-left">Md Nasir Fardoush</span>
						<span class="pull-right" > {{ $article->created_at->diffForHumans() }}</span>
					</div>
					<div class="panel-body" >
						{{ $article->content }}
						
					</div>
					<div class="panel-footer clearfix" style="background-color: #fff;" >
						<i class="fa fa-thumbs-up pull-right text-success"></i>
					</div>
			</div>
		</div>	
	</div>	
@endsection