@extends('layouts.app')

@section('content')
	
		@forelse($articles as $article)
		<div class="row">
			<div class="col-md-4 col-lg-4 col-md-offset-4 co-lg-offset-4" >
				<div class="panel panel-defult">
					<div class="panel panel-heading text-center">
						<span>Md Nasir Fardoush</span>
						<span class="pull-right" > {{ $article->created_at->diffForHumans() }}</span>

						echo ""
						</div>
					<div class="panel-body" >
						{{ $article->ShortContent }}
						<a href="/articles/{{ $article->id }}">Read more</a>
					</div>
					<div class="panel-footer clearfix" style="background-color: #fff;" >
						<i class="fa fa-thumbs-up pull-right text-success"></i>
					</div>
			</div>
		</div>	
	</div>	
		@empty
			Articles not found
		@endforelse
		<div class="row">
			<div class="col-md-4 col-lg-4 col-md-offset-4 co-lg-offset-4">
				{{ $articles->links() }}
			</div>
		</div>
@endsection